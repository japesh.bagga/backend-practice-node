import mongoose from 'mongoose'
import Todo from './models/todos';   
export const readTodos = async(req, res)=>{
    try{
        const todos = await Todo.find();
        res.status(200).json(todos);
    }
    catch(error){
        res.status(200).json({ error: error.message })
    }
}
export const createTodos = async(req, res)=>{
    const todo = new Todo(req.body);
    try{
        const todos = await Todo.find();
        res.status(201).json(todos);
    }
    catch(error){
        res.status(409).json({ error: error.message })
    }
}
