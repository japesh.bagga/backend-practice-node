import express from "express"
import mongoose from 'mongoose'
import cors from 'cors'
import dotenv from "dotenv";
import todo from './models/todo.js'
const app = express()
const port = process.env.PORT || 8080;

dotenv.config();

app.use('/todo', todo)

var mongoDB = 'mongodb+srv://jp:jp@cluster0.tmvff.mongodb.net/sample_airbnb?retryWrites=true&w=majority';
mongoose.connect(mongoDB, { useNewUrlParser: true , useUnifiedTopology: true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(cors());

app.get('', (req, res) => {
  console.log(process.env.PORT)
  res.send('Connected Successfully !')
})

app.get('/hello', (req, res)=>{
    res.send('Disconnected')
})

//  app.post('/get-news',(req,res)=>{
//     // const data = fetch_data()
//     res.send({body:"data"})
// })

app.get('/get-news',(req,res)=>{
  // const data = fetch_data()
  res.send("Data")
})

app.listen(port, (err) => {
  if (err) console.log(err);
  console.log(`Example app listening on port ${port}`)
})

// async function fetch_data(){
//     const response = await fetch(`https://newsapi.org/v2/top-headlines?sortBy=popularity&country=in&apiKey=${process.env.API_KEY}`)
//     const data = await response.json()
//     return data.articles[0]
// }