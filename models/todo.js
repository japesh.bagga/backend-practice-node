import mongoose from 'mongoose';
const schema = mongoose.Schema;

const todoSchema = new schema( {
    title:{
        type: String,
        required: true
    },  
    content: String,
    Number: Number
}, {timestamps: true});

const todo = mongoose.model('Todo', todoSchema)

export default todo